<?php

namespace app\controllers;

use yii\web\Controller;

class MyController extends Controller
{
    public function actionIndex($fio = 'Укажите фио')
    {
        return $fio;
    }

    public function actionAbout()
    {
        return $this->render('about' ,[
            'ololoshka' => 'gg'

        ]);

    }

    public function actionStyle()
    {
        return $this->render('style' ,[
            'ohlystyle' => 'green_shirt'

        ]);

    }

    public function actionMe()
    {
        return $this->render('me' ,[
            'memka' => 'BashlatorVanLav'
        ]);

    }

    public function actionPypsik()
    {
        return $this->render('pypsik' ,[
            'u' => 'кис-кис-кис'
        ]);

    }
}

